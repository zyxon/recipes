import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportRecipesComponent } from './import-recipes.component';

describe('ImportRecipesComponent', () => {
  let component: ImportRecipesComponent;
  let fixture: ComponentFixture<ImportRecipesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportRecipesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportRecipesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
