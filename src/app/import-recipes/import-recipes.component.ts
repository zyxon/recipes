import { RecipeService } from './../recipe-service.service';
import { Ingredient } from './../entities/ingredient';
import { Recipe } from './../entities/recipe';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-import-recipes',
  templateUrl: './import-recipes.component.html',
  styleUrls: ['./import-recipes.component.css']
})
export class ImportRecipesComponent implements OnInit {

  inputText: string = null;

  constructor(private _recipeService: RecipeService) {
    this.inputText = null;
  }

  ngOnInit() {
    this.inputText = null;
  }

  importClickHandler(): void {
    var parsed: Recipe[];
    try {
      parsed = JSON.parse(this.inputText);

      for (var i = 0; i < parsed.length; i++){
        if (!this.validateRecipe(parsed[i])){
          throw new Error();
        }
      }

      // az összes recept jó volt

      for (var i = 0; i < parsed.length; i++){
        var rec = parsed[i];
        rec.id = null;
        this._recipeService.addRecipe(rec);
      }
    } catch(e) {
      alert("ParseError")
    }
  }

  validateRecipe(r: Object): boolean{
    if (
      typeof r["id"] !== "number" &&
      typeof r["name"] !== "string" &&
      typeof r["image"] !== "string" &&
      typeof r["forPersons"] !== "number" &&
      typeof r["description"] !== "string" &&
      !(Array.isArray(r["ingredients"]))
    ){
      // nincsenek kitöltve az attribútumok
      return false;
    } else {
      // ki vannak töltve az attribútumok
      var ing: Ingredient[] = r["ingredients"];
      for (var i = 0; i < ing.length; i++){
        if (!(typeof ing[i]["name"] === "string")){
          return false;
        }
      }
    }
    return true;
  }

}
