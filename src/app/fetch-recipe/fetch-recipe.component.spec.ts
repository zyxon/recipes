import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchRecipeComponent } from './fetch-recipe.component';

describe('FetchRecipeComponent', () => {
  let component: FetchRecipeComponent;
  let fixture: ComponentFixture<FetchRecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FetchRecipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FetchRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
