import { Recipe } from './../entities/recipe';
import { RecipeService } from './../recipe-service.service';
import { FormsModule } from '@angular/forms';
import { RecipeFetchService } from './../recipe-fetch.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fetch-recipe',
  templateUrl: './fetch-recipe.component.html',
  styleUrls: ['./fetch-recipe.component.css']
})
export class FetchRecipeComponent implements OnInit {

  recipeURL: string = "";
  selectedSource: string = "ReceptNeked";
  avSources: string[] = [];

  loading: boolean = false;
  success: boolean = false;
  error: boolean = false;

  constructor(
    private _fetchService: RecipeFetchService,
    private _recipeService: RecipeService
  ) {
    this._fetchService.onRequestDone.subscribe((key) => {
      this.loading = false;
      console.log("in component");
      console.log(this._fetchService.requestMap[key]);
      
      try {
        var gotRecipe: Recipe = this._fetchService.requestMap[key];
        this._recipeService.addRecipe(gotRecipe);
        this._recipeService.onChange.emit();
        delete(this._fetchService.requestMap[key]);

        if (gotRecipe && gotRecipe !== null) {
          this.success = true;
        } else {
          this.error = true;
        }
        
      } catch (e) {
        this.error = true;
      }
      
    });
    this.avSources = this._fetchService.getSources();
    this.selectedSource = this.avSources[0];

    console.log(this.avSources);
    console.log(this.selectedSource);
  }

  ngOnInit() {

  }

  fetchRecipeViaService(): void {
    this._fetchService.fetchRecipe(this.selectedSource, this.recipeURL);
    this.loading = true;
    this.success = false;
    this.error = false;

  }

}
