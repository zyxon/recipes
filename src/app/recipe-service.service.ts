import { Ingredient } from './entities/ingredient';
import { Recipe } from './entities/recipe';
import { Injectable, Output, EventEmitter } from '@angular/core';


@Injectable()
export class RecipeService {

  onChange = new EventEmitter();
  onAddToAngTodos = new EventEmitter();

  private recipes: Recipe[] = null;

  constructor() {

  }

  getRecipes(): Promise<Recipe[]> {
    var storedRecipes: string = localStorage.recipes;
    try {
      this.recipes = JSON.parse(storedRecipes);
    } catch (e) {
      console.log("service get exc");
      this.recipes = [];
      localStorage.recipes = JSON.stringify(this.recipes);
    }

    return Promise.resolve(this.recipes);
  }

  getRecipe(needle: number): Recipe {
    for (var i = 0; i < this.recipes.length; i++) {
      var r = this.recipes[i];
      if (r.id === needle) {
        return this.recipes[i];
      }
    }

    return null;
  }

  addRecipe(recipe: Recipe): Promise<void> {
    if (recipe.id === null) {
      if (this.recipes.length === 0) {
        recipe.id = 1;
      } else {
        var currentMax: number = this.recipes[0].id;

        for (var i = 0; i < this.recipes.length; i++) {
          var r = this.recipes[i];
          if (r.id > currentMax) {
            currentMax = r.id;
          }
        }

        recipe.id = currentMax + 1;
      }
    }

    this.recipes.push(recipe);
    localStorage.recipes = JSON.stringify(this.recipes);
    return Promise.resolve();
  }

  deleteRecipe(id: number): void {
    var delIdx: number;

    for (var i = 0; i < this.recipes.length; i++) {
      var r = this.recipes[i];
      if (r.id === id) {
        delIdx = i;
        console.log("found del: " + delIdx);
        break;
      }
    }

    this.recipes.splice(delIdx, 1);
    console.log(this.recipes);
    localStorage.recipes = JSON.stringify(this.recipes);

    this.onChange.emit(null);
  }

  addToAngTodos(rec: Recipe): void {
    console.log("add to angtodos");
    var todosStr: string = localStorage.AngTodos;
    var todos: string[];
    try {
      todos = JSON.parse(todosStr);
    } catch (e) {
      todos = [];
    }

    for (var i = 0; i < rec.ingredients.length; i++) {
      var ing = rec.ingredients[i];
      var todoStr: string = "";
      todoStr += ing["name"];
      if (typeof ing["amount"] !== "undefined" && ing["amount"] !== null) {
        todoStr += ": " + ing["amount"];
      }
      if (typeof ing["unit"] !== "undefined" && ing["unit"] != null) {
        todoStr += " " + ing["unit"];
      }

      todos.push(todoStr);
    }

    localStorage.AngTodos = JSON.stringify(todos);
    console.log("done");
    this.onAddToAngTodos.emit()
  }

  updateRecipe(updatedRecipe: Recipe) {
    var foundIdx: number = null;
    for (var i = 0; i < this.recipes.length; i++) {
      var rec = this.recipes[i];
      if (rec.id === updatedRecipe.id) {
        foundIdx = i;
        break;
      }
    }

    if (foundIdx !== null) {
      console.log("found edited idx");
      this.recipes[foundIdx] = updatedRecipe;

      localStorage.recipes = JSON.stringify(this.recipes);

      this.onChange.emit();
    } else {
      console.log("edited idx not found");
    }
  }

}
