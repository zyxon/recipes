import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nltobr'
})
export class NltobrPipe implements PipeTransform {

  transform(value: string): any {
    return value.replace('\n', '<br/>');
  }

}
