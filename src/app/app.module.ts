import { RecipeFetchService } from './recipe-fetch.service';
import { RecipeService } from './recipe-service.service';
import { Routes, Router, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';
import { NltobrPipe } from './nltobr.pipe';
import { ExportRecipesComponent } from './export-recipes/export-recipes.component';
import { ImportRecipesComponent } from './import-recipes/import-recipes.component';
import { EditRecipeComponent } from './edit-recipe/edit-recipe.component';
import { FetchRecipeComponent } from './fetch-recipe/fetch-recipe.component';

const routes: Routes = [
  {
    path: 'recipe/:id',
    component: RecipeDetailComponent
  }, 
  {
    path: 'export',
    component: ExportRecipesComponent
  },
  {
    path: 'import',
    component: ImportRecipesComponent 
  },
  {
    path: 'edit/:id',
    component: EditRecipeComponent
  },
  {
    path: 'fetch',
    component: FetchRecipeComponent
  },
  {
    path: 'add',
    component: NewRecipeComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    NewRecipeComponent,
    NltobrPipe,
    ExportRecipesComponent,
    ImportRecipesComponent,
    EditRecipeComponent,
    FetchRecipeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule
  ],
  providers: [RecipeService, RecipeFetchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
