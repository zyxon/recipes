import { RecipeDetailComponent } from './../recipe-detail/recipe-detail.component';
import { Recipe } from './../entities/recipe';
import { RecipeService } from './../recipe-service.service';
import { Component, OnInit } from '@angular/core';
import { Router, Routes, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  searchTxt: string = "";
  recipes: Recipe[] = [];
  selectedRecipe: Recipe;

  constructor(private _recipeService: RecipeService,
    private _router: Router) { }

  ngOnInit() {
    this.getRecipesFromStorage();
    this._recipeService.onChange.subscribe(() => this.getRecipesFromStorage());
  }

  private getRecipesFromStorage(): void{
    this._recipeService.getRecipes().then((recipes) => {
      this.recipes = recipes;
    });
  }

  onSelect(recipe: Recipe): void {
    this.selectedRecipe = recipe;

    this._router.navigate([
      "/recipe",
      recipe.id
    ]);
  }

  matchRecipes(): Recipe[] {
    var matched: Recipe[] = [];

    for (var i = 0; i < this.recipes.length; i++){
      var r = this.recipes[i];
      var rNameUc: string = r.name.toUpperCase();
      if (rNameUc.indexOf(this.searchTxt.toUpperCase()) !== -1){
        matched.push(r);
      }
    }

    return matched;
  }

}
