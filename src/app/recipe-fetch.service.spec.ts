import { TestBed, inject } from '@angular/core/testing';

import { RecipeFetchService } from './recipe-fetch.service';

describe('RecipeFetchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipeFetchService]
    });
  });

  it('should be created', inject([RecipeFetchService], (service: RecipeFetchService) => {
    expect(service).toBeTruthy();
  }));
});
