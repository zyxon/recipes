import { Ingredient } from './../entities/ingredient';
import { Recipe } from './../entities/recipe';
import { RecipeService } from './../recipe-service.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit, AfterViewInit {

  recipeTBE: Recipe;

  constructor(
    private _route: ActivatedRoute,
    private _recipeService: RecipeService
  ) {
    this._route.params.subscribe(
      params => {
        var srvRecipe = _recipeService.getRecipe(parseInt(params["id"]));
        //this.recipeTBE = srvRecipe;
        // this is a pretty bad way to copy an object, need to implement a copy constructor later
        this.recipeTBE = JSON.parse(JSON.stringify(srvRecipe));

        console.log("active recipe in edit");
        console.log(this.recipeTBE);
      }
    );
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    jQuery(".tt").tooltip();
    jQuery(".tt").click(function () {
      jQuery(this).tooltip("hide");
    })
  }

  addIngredient(): void {
    this.recipeTBE.ingredients.push(new Ingredient());
  }

  removeIngredient(index: number): void {
    this.recipeTBE.ingredients.splice(index, 1);
  }

  saveEdit(): void {
    this._recipeService.updateRecipe(this.recipeTBE);
  }

  cancelEdit(): void {
    this._recipeService.onChange.emit();
  }

}
