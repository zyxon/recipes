import { Ingredient } from "./ingredient";

export class Recipe {
    id: number;
    name: string;
    image: any;
    ingredients: Ingredient[];
    forPersons: number;
    description: string;

    constructor() {
        this.id = null;
        this.name = null;
        this.ingredients = [];
        this.forPersons = null;
        this.description = null;
    }
}