export class RecipeSource {
  name: string;
  url: string;
  nameSelector: string;
  descriptionSelector: string;
  ingredientsSelector: string;
  forPersonsSelector: string;
  imageSelector: string;
}