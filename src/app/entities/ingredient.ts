export class Ingredient {
  name: string;
  amount: number;
  unit: string;

  constructor() {
    this.name = null;
    this.amount = null;
    this.unit = null;
  }
}