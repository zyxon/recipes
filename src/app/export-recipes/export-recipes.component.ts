import { RecipeService } from './../recipe-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-export-recipes',
  templateUrl: './export-recipes.component.html',
  styleUrls: ['./export-recipes.component.css']
})
export class ExportRecipesComponent implements OnInit {

  exportedRecipesText: string = null;

  constructor( private _recipeService: RecipeService) { 
    this.exportedRecipesText = null;
   }

  ngOnInit() {
    this.exportedRecipesText = null;
  }

  getExportedRecipes() : void {
    this._recipeService.getRecipes().then((recipes) => {
      this.exportedRecipesText = JSON.stringify(recipes);
    });
  }

}
