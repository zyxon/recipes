import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportRecipesComponent } from './export-recipes.component';

describe('ExportRecipesComponent', () => {
  let component: ExportRecipesComponent;
  let fixture: ComponentFixture<ExportRecipesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportRecipesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportRecipesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
