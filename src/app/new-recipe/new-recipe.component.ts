import { Router } from '@angular/router';
import { RecipeService } from './../recipe-service.service';
import { Ingredient } from './../entities/ingredient';
import { Recipe } from './../entities/recipe';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';

declare var jQuery: any;

@Component({
  selector: 'new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit {

  recipeTBA: Recipe = new Recipe();

  constructor(
    private _recipeService: RecipeService,
    private _router: Router
  ) {
    this.recipeTBA = new Recipe();
  }

  ngOnInit() {
    this.recipeTBA = new Recipe();
  }

  closeNewRecipe(): void {
    this.recipeTBA = new Recipe();
  }

  submitAddedRecipe(): void {
    if (this.recipeTBA.name && this.recipeTBA.name.trim() !== "") {
      this.recipeTBA.name = this.recipeTBA.name.trim();
      this._recipeService.addRecipe(this.recipeTBA);
      this._recipeService.onChange.emit();

      this._router.navigate([
        "/"
      ]);
    } else {
      jQuery("#name-needed-modal").modal("show");
    }
  }

  debug(): void {
    console.log(this.recipeTBA);
  }

  addIngredient(): void {
    this.recipeTBA.ingredients[this.recipeTBA.ingredients.length] = new Ingredient();
  }

  removeIngredient(index: number): void {
    this.recipeTBA.ingredients.splice(index, 1);
  }

}
