import { Recipe } from './../entities/recipe';
import { RecipeService } from './../recipe-service.service';
import { Component, OnInit, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

declare var jQuery: any;

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, AfterViewInit{

  activeRecipe: Recipe;
  activeTab: string = "ingredients";

  @Output() onDelete = new EventEmitter();

  constructor(
    private _recipeService: RecipeService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _location: Location
  ) {

    this._route.params.subscribe(
      params => {
        var srvRecipe = _recipeService.getRecipe(parseInt(params["id"]));
        this.activeRecipe = srvRecipe;
      }
    );

    this._recipeService.onAddToAngTodos.subscribe(() => console.log("Recipe ingredients exported to AngTodo"));

  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    // this is where we hook the bootstrap/jq events
    jQuery(".tt").tooltip();
    jQuery(".tt").click(function(){
      jQuery(this).tooltip("hide");
    })
  }

  deleteThisRecipe(): void {
    this._recipeService.deleteRecipe(this.activeRecipe.id);
    
  }

  addToAngTodos(): void {
    this._recipeService.addToAngTodos(this.activeRecipe);
    jQuery("#angtodo-added").modal("show");
  }

  onEdit(): void {
    this._router.navigate([
      '/edit',
      this.activeRecipe.id
    ]);
  }

}
