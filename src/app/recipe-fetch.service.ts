import { Ingredient } from './entities/ingredient';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RecipeSource } from './entities/recipe-source';
import { Injectable, EventEmitter } from '@angular/core';
import { Recipe } from './entities/recipe';

@Injectable()
export class RecipeFetchService {

  private CORS: string = "https://cors-anywhere.herokuapp.com";

  // document.querySelectorAll(...)

  onRequestDone = new EventEmitter();
  requestMap = {};

  sources: RecipeSource[] = [
    {
      name: "ReceptNeked",
      url: "https://receptneked.hu",
      nameSelector: "#recipe-title",
      forPersonsSelector: null,
      descriptionSelector: "#recipe-story p",
      imageSelector: ".recipe-main-img",
      ingredientsSelector: "#ingredients-list ul li span"
    },
  ]

  constructor(private _http: HttpClient) { }

  getSources(): string[] {
    var ret: string[] = [];
    for (var i = 0; i < this.sources.length; i++){
      ret.push(this.sources[i].name);
    }

    return ret;
  }

  fetchRecipe(sourceName: string, url: string): void {

    console.log("FetchService, " + sourceName + " :: " + url);
    if (sourceName && url && sourceName !== "" && url !== "") {
      var src: RecipeSource = null;

      for (var i = 0; i < this.sources.length; i++) {
        if (this.sources[i].name === sourceName) {
          src = this.sources[i];
          break;
        }
      }

      if (src !== null && url.indexOf(src.url) !== -1) {
        console.log("source valid");
        this._http.get(this.CORS + "/" + url, {
          headers: new HttpHeaders()
            .set("X-Requested-With", "jancsika"),
          responseType: 'text'
        }).subscribe((response) => {
          console.log("SUCCESS");
          //console.log(response);

          var returnRecipe = new Recipe();


          var dom = document.createElement("html");
          dom.innerHTML = response;


          // console.log(dom.querySelector(src.nameSelector).innerHTML);
          // console.log(dom.querySelector(src.imageSelector).getAttribute("src"));
          
          var description: string = "";
          var siteDescription = dom.querySelectorAll(src.descriptionSelector);
          for (var k = 0; k < siteDescription.length; k++){
            var p = siteDescription[k];
            description += (p.innerHTML + "\n");
          }
          // console.log(description);

          var ingredients: Ingredient[] = [];
          var siteIngredients = dom.querySelectorAll(src.ingredientsSelector);
          for (var k = 0; k < siteIngredients.length; k++){
            var ing = new Ingredient();
            ing.name = siteIngredients[k].innerHTML;

            ingredients[k] = ing;
          }
          
          returnRecipe.name = dom.querySelector(src.nameSelector).innerHTML;
          returnRecipe.description = description;
          returnRecipe.image = dom.querySelector(src.imageSelector).getAttribute("src");
          returnRecipe.forPersons = null;
          returnRecipe.ingredients = ingredients;

          this.requestMap[url] = returnRecipe;
          this.onRequestDone.emit(url);
          
        }, err => {
          console.log("error");
          console.log(err);
        });
      }
    }
  }



}
